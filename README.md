Cozmo Yachts is the premium yacht charter company in Dubai. Offers a wide range of luxury yachts for charter. If you are looking for exclusive yachting experience in Dubai Cozmo Yachts is there for you 24/7, Just call us or submit a yacht booking form available on our website.

Address: 803 Silver Tower, Business Bay, Dubai 238065, UAE

Phone: +971 52 944 0222
